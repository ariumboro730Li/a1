<?php
 // DB connection info
 $host = "localhost";
 $user = "root";
 $pwd = "";
 $db = "db_percobaan";
 // Connect to database.
 try
 {
     $conn = new PDO( "mysql:host=$host;dbname=$db", $user, $pwd);
     $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
 }
 catch(Exception $e)
 {
     die(var_dump($e));
 }
 if(!empty($_POST))
{
  try
    {
        $name = $_POST['number'];
        $email = $_POST['name'];
        $date = date("Y-m-d");
        // Insert data
        $sql_insert = "INSERT INTO registration_tbl (name, email, date) 
                        VALUES (?,?,?)";
        $stmt = $conn->prepare($sql_insert);
        $stmt->bindValue(1, $name);
        $stmt->bindValue(2, $email);
        $stmt->bindValue(3, $date);
        $stmt->execute();
    }
        catch(Exception $e)
        {
            die(var_dump($e));
        }
        echo "<h3>Your're registered!</h3>";
}
$sql_select = "SELECT * FROM registration_tbl";
 $stmt = $conn->query($sql_select);
 $registrants = $stmt->fetchAll(); 
 if(count($registrants) > 0)
 {
     echo "<h2>People who are registered:</h2>";
     echo "<table>";
     echo "<tr><th>NIK</th>";
     echo "<th>Name</th>";
	 echo "<tr><th>NIK</th>";
     echo "<th>Name</th>";
	 echo "<tr><th>NIK</th>";
     echo "<th>Name</th>";
     echo "<th>Date</th></tr>";
     foreach($registrants as $registrant)
     {
         echo "<tr><td>".$registrant['name']."</td>";
         echo "<td>".$registrant['email']."</td>";
         echo "<td>".$registrant['date']."</td></tr>";
     }
     echo "</table>";
 }
 
 else
 {
     echo "<h3>No one is currently registered.</h3>";
 }
 
 ?>
 
 </body>
 </html>