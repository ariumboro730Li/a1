<!DOCTYPE html>
<html>
<head>
 <title> Input Data KTP </title>
 
 <style>
 body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background-color:white;
	margin: 0;
	padding: 0;
	color: #fff;
}
 .container {
	width: 500px;
	background-color: #ff5500;
	margin: 0 auto;
	
 }
	 h1
	 { color:#fff;
	 font-size:1.3em;
	 text-align:center;
	 position:relative;}
	 
 ol li
 {	
 font-family:3em;
 width:80%;
 f0nt-family:times new roman;
 margin-left:50px;
 }
 
 input { font-size:1em;
 margin-left:60px;
	 border:1.5px solid;
 border-color:#000;
	 width:300px;
	 height:30px;}
	 
	 textarea{ font-size:1em;
 margin-left:60px;
	 border:1.5px solid;
 border-color:#000;
	 width:300px;
	 height:90px;}
	 
	 select {
		 margin-left:60px;
		 border:1.5px solid;
		 font-size:1em;
 border-color:#000;
	 width:302px;
	 height:32px;}
	 
	
	button {background:blue;
	text-align:center;
	width:175px;
	height:30px;
	font-size:1em;
	margin-left:120px;
	display:inline;
	color:white;}

	input[type=radio   ]:not(old){
  width     : 1em;
  margin    : 0;
  padding   : 0;
  font-size : 0.8em;
  opacity   : 0;
}

input[type=radio   ]:not(old){
  width     : 2em;
  margin    : 0;
  padding   : 0;
  font-size : 1em;
  opacity   : 0;
}

input[type=radio   ]:not(old) + label{
  display      : inline-block;
  margin-left  : -2em;
  line-height  : 1.5em;
}

input[type=radio   ]:not(old) + label > span{
  display          : inline-block;
  width            : 0.875em;
  height           : 0.875em;
  margin           : 0.25em 0.5em 0.25em 0.25em;
  border           : 2px solid #000;
  border-radius    : 0.25em;
  background       : #fff;
  vertical-align   : bottom;
}

input[type=radio   ]:not(old):checked + label > span{
  background-image :    -moz-linear-gradient(rgb(224,224,224),rgb(240,240,240));
  background-image :     -ms-linear-gradient(rgb(224,224,224),rgb(240,240,240));
  background-image :      -o-linear-gradient(rgb(224,224,224),rgb(240,240,240));
  background-image : -webkit-linear-gradient(rgb(224,224,224),rgb(240,240,240));
  background-image :         linear-gradient(rgb(224,224,224),rgb(240,240,240));
}

input[type=radio]:not(old):checked +  label > span > span{
  display          : block;
  width            : 0.5em;
  height           : 0.5em;
  margin           : 0.125em;
  border           : 0.0625em solid rgb(115,153,77);
  border-radius    : 0.125em;
  background       : black;
}


	
 </style>
</head>
<body>
<div class="container">
<form  action="action.php" method='POST'>
<br>

	<h1>  Masukan Data KTP Anda </h1>
	
<ol>
	
	<li> NIK </li><br>
		<input type="number"  name="number" min="1111111111111111" max="9999999999999999" id="nik" required
			oninvalid="this.setCustomValidity('Masukan 16 angka NIK Anda')" oninput="setCustomValidity('')"> 
	<br><br>
	
	<li> Nama Lengkap </li><br>
		<input type="text" name="name" id="name" required>
	<br><br>
	
	<li> Tempat & Tanggal Lahir </li><br>
		<select type="text" name="cars" id="cars">
	
		<option value="Jakarta">Jakarta</option>
		<option value="Bogor">Bogor</option>
		<option value="Depok">Depok</option>
		<option value="Tangerang">Tangerang</option>
		<option value="Bekasi">Bekasi</option>
		<option value="Bandung">Bandung</option>
		<option value="Banten">Banten</option>
		<option value="Surabaya">Surabaya</option>
		<option value="Yogyakarta">Yogyakarta</option>
		<option value="Semarang">Semarang</option>
		<option value="Sumatera Utara">Sumatera Utara</option>
		<option value="Sumatera Barat">Sumatera Barat</option>
		<option value="Sumatera Selatan">Sumatera Selatan</option>
		<option value="Kalimantan Timur">Kalimantan Timur</option>
		<option value="Kalimantan Barat">Kalimantan Barat</option>
		<option value="Kalimantan Selatan">Kalimantan Selatan</option>
		</select>
	<input type="date" name="date" max="2005-12-31" required>
	<br><br>
	
	
	<li> Jenis Kelamin 
        <input id="radio1" type="radio" name="radio1" value="Laki-Laki" required><label for="radio1"><span><span></span></span>L</label>
        <input id="radio2" type="radio" name="radio1" value="Perempuan" required ><label for="radio2" ><span><span></span></span>P</label>
		</li>
      
	  <li> Gol Darah 
       <input id="radio1" type="radio" name="radio3" value="A" required><label for="radio1"><span><span></span></span>A</label>
       <input id="radio1" type="radio" name="radio3" value="B" required><label for="radio1"><span><span></span></span>B</label>
       <input id="radio2" type="radio" name="radio3" value="AB" required ><label for="radio2" ><span><span></span></span>AB</label>
       <input id="radio2" type="radio" name="radio3" value="O" required ><label for="radio2" ><span><span></span></span>O</label>
      </li><br>
      
	   <li> Agama </li><br>
		<select name="religion" >
		<option value="Islam">Islam</option>
		<option value="Katolik">Katolik</option>
		<option value="Protestan">Protestan</option>
		<option value="Hindu">Hindu</option>
		<option value="Budha">Budha</option>
	</select>
	<br><br>
	
	<li> Alamat</li><br><textarea class="di" name="alamat" required></textarea>
	<br><br>
	
	<li> Pekerjaan <br>
      <div>
        <input id="radio1" type="radio" name="radio" value="Pegawai Pemerintah" required><label for="radio1"><span><span></span></span>Pegawai Pemerintah</label>
      </div>
      <div>
        <input id="radio2" type="radio" name="radio" value="Pegawai Swasta" required><label for="radio2"><span><span></span></span>Pegawai Swasta</label>
        <input id="radio3" type="radio" name="radio" value="Wirausaha" required><label for="radio3"><span><span></span></span>Wirausaha</label>
      </div>
	<br></li>
	
	  <li> Status Pernikahan
      <div>
        <input id="radio1" type="radio" name="radio2" value="Sudah Menikah" required><label for="radio1"><span><span></span></span>Sudah Menikah</label><br>
        <input id="radio2" type="radio" name="radio2" value="Belum Menikah" required ><label for="radio2" ><span><span></span></span>Belum Menikah</label><br>
        <input id="radio2" type="radio" name="radio2" value="Duda / Janda" required ><label for="radio2" ><span><span></span></span>Duda / Janda</label><br>
      </div></li>
	  
	 <li> Kewarganegaraan 
        <input id="radio1" type="radio" name="warga" value="Indonesia" required><label for="radio1"><span><span></span></span>WNI</label>
        <input id="radio2" type="radio" name="warga" value="Asing" required ><label for="radio2" ><span><span></span></span>WNA</label></li>
		
		<li> Berlaku Hingga
        <input id="radio2" type="radio" name="berlaku" value="Seumur Hidup" checked ><label for="radio2" ><span><span></span></span>Seumur Hidup</label>
		</li><br><br>
		
		<div><button type="submit" style="text-align:center";><b>Submit Data</b></button></div>
		<br>
        <div><button type="reset" style="text-align:center";><b>Reset Data</b></button></div>
		<br>
</ul>
</form>
</div>
</body>
</html>




