<!DOCTYPE html>
<html>
<head>
	<title>exercise php html forms</title>
    <style>
	
	body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background-color:white;
	margin: 0;
	padding: 0;
	color: black;
}

.container {
	width: 800px;
	background-color:#FFF;
	margin: 0 auto;
	
 }
  h1
	 { color:black;
	 font-size:3em;
	 text-align:center;
	 position:relative;}
	 
	h3
	 { color:#black;
	 font-size:1.3em;
	 text-align:center;
	 position:relative;}
	 
	 p {  color:#black;
	 font-size:1.3em;
	 text-align:left;
	 position:relative;
	 margin-left:60px;}
	 
input { font-size:1em;
 margin-left:60px;
	 border:1.5px solid;
 border-color:#000;
	 width:300px;
	 height:30px;}
	 
	 textarea{ font-size:1em;
 margin-left:60px;
	 border:1.5px solid;
 border-color:#000;
	 width:300px;
	 height:90px;}
	 
	 select {
		 margin-left:60px;
		 border:1.5px solid;
		 font-size:1em;
 border-color:#000;
	 width:302px;
	 height:32px;}
	 
	 button.submit {background:red;
	text-align:center;
	width:125px;
	height:30px;
	font-size:1em;
	margin-left:100px;
	display:inline;
	color:white;}
	 button.reset{background:blue;
	text-align:center;
	width:125px;
	height:30px;
	font-size:1em;
	margin-left:20px;
	display:inline;
	color:white;}
	
	

	input[type=radio   ]:not(old){
  width     : 1em;
  margin-left   : 60px;
  padding   : 0;
  font-size : 0.8em;
  opacity   : 0;
}

input[type=radio   ]:not(old){
  width     : 2em;
  margin    : 0;
  padding   : 0;
  font-size : 1em;
  opacity   : 0;
}

input[type=radio   ]:not(old) + label{
  display      : inline-block;
  margin-left  : -2em;
  line-height  : 1.5em;
}

input[type=radio   ]:not(old) + label > span{
  display          : inline-block;
  width            : 0.875em;
  height           : 0.875em;
  margin           : 0.25em 0.5em 0.25em 0.25em;
  border           : 2px solid #000;
  border-radius    : 0.25em;
  background       : #fff;
  vertical-align   : bottom;
}

input[type=radio   ]:not(old):checked + label > span{
  background-image :    -moz-linear-gradient(rgb(224,224,224),rgb(240,240,240));
  background-image :     -ms-linear-gradient(rgb(224,224,224),rgb(240,240,240));
  background-image :      -o-linear-gradient(rgb(224,224,224),rgb(240,240,240));
  background-image : -webkit-linear-gradient(rgb(224,224,224),rgb(240,240,240));
  background-image :         linear-gradient(rgb(224,224,224),rgb(240,240,240));
}

input[type=radio]:not(old):checked +  label > span > span{
  display          : block;
  width            : 0.5em;
  height           : 0.5em;
  margin           : 0.125em;
  border           : 0.0625em solid rgb(115,153,77);
  border-radius    : 0.125em;
  background       : #C00;
}
	</style>
</head>
<body>
<div class="container">

	<h1> Week Exercise : PHP MYSQL </h1>
    <h3> Masukan data Anda dan klik <b style="color:red"> SUBMIT </b></h3>
<form action="database.php"method='POST'>
	<p><b>Nik</b></p>
	<input type="text"name="nik">
    <p><b>Nama lengkap</b></p>
	<input type="text"name="nama_lengkap">
	<p><b>Alamat</b></p>
	<textarea name="alamat"></textarea>
    <p><b>Kelurahan</b></p>
     <input type="text"name="kelurahan">
    <p><b>Kecamatan</b></p>
     <input type="text"name="kecamatan">
    <p><b>Tempat & Tgl Lahir</b></p>
	<input type="text"name="tempat_lahir">
    <input type="date"name="tgl_lahir">
    <p><b>Jenis Kelamin</b>
	<input id="radio1" type="radio" name="jenis_kelamin" value="Laki-Laki" required><label for="radio1"><span><span></span></span>L</label>
        <input id="radio2" type="radio" name="jenis_kelamin" value="Perempuan" required ><label for="radio2" ><span><span></span></span>P</label></p>
       <p><b>Gol Darah</b>
        <input id="radio1" type="radio" name="gol_darah" value="A" required><label for="radio1"><span><span></span></span>A
        </label>
       <input id="radio1" type="radio" name="gol_darah" value="B" required><label for="radio1"><span><span></span></span>B
       </label>
       <input id="radio2" type="radio" name="gol_darah" value="AB" required ><label for="radio2" ><span><span></span></span>AB
       </label>
       <input id="radio2" type="radio" name="gol_darah" value="O" required ><label for="radio2" ><span><span></span></span>O
       </label>
     </p>
     <p><b> Agama </b></p>
     <select name="agama" >
		<option value="Islam">Islam</option>
		<option value="Katolik">Katolik</option>
		<option value="Protestan">Protestan</option>
		<option value="Hindu">Hindu</option>
		<option value="Budha">Budha</option>
	</select>
    <p><b> Status Pernikahan </b><br>
    <input id="radio1" type="radio" name="status" value="Sudah Menikah" required><label for="radio1"><span><span></span></span>Sudah Menikah</label>
        <input id="radio2" type="radio" name="status" value="Belum Menikah" required ><label for="radio2" ><span><span></span></span>Belum Menikah</label>
        <input id="radio2" type="radio" name="status" value="Duda / Janda" required ><label for="radio2" ><span><span></span></span>Duda / Janda</label></p>
        
        <p><b>Pekerjaan</b> <br>
         <input id="radio1" type="radio" name="pekerjaan" value="Pegawai Pemerintah" required><label for="radio1"><span><span></span></span>Pegawai Pemerintah</label>
        <input id="radio2" type="radio" name="pekerjaan" value="Pegawai Swasta" required><label for="radio2"><span><span></span></span>Pegawai Swasta</label>
        <input id="radio3" type="radio" name="pekerjaan" value="Wirausaha" required><label for="radio3"><span><span></span></span>Wirausaha</label></p>
        
        <p><b>Kewarganegaraan</b> 
                <input id="radio1" type="radio" name="kewarganegaraan" value="Indonesia" required><label for="radio1"><span><span></span></span>WNI</label>
        <input id="radio2" type="radio" name="kewarganegaraan" value="Asing" required ><label for="radio2" ><span><span></span></span>WNA</label></p>
       <br><br>
		<button class="submit" type="submit" style="text-align:center";><b>Submit Data</b></button>
        <button class="reset" type="reset" style="text-align:center";><b>Reset Data</b></button> <b> Created By : Ari Umboro<b> <br><br>
        </form>
        </div>
        
</html>