<?php
error_reporting(0);
?>


<!DOCTYPE html>
<html>
<head>
	<title> Mrtampan.co</title>
	
	<link rel = "stylesheet" type = "text/css" href = "assets/css/bootstrap.css">
		
	<link rel = "stylesheet" type = "text/css" href = "assets/js/jquery-ui/jquery-ui.css">

	<script type="text/javascript" src="assets/js/jquery.js"></script>

	<script type = "text/javascript" src = "assets/js/bootstrap.js"></script>

	<script type = "text/javascript" src = "assets/js/jquery-ui/jquery-ui.js"></script>
	


</head>

<body  class="body" style="font-family:nyala;">

<div class="container">
  <div class="row">
  <div class="col-md-12">

	<nav class="navbar navbar-default" role="navigation" style="background-color:white; border:none;"> 
		<div class="navbar-header" > 
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
				<span class="sr-only">Menu</span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
			</button> 
			<a class="navbar-brand" href="#" ><b style="font-size:2em; color:#ff5500">Mrtampan</b><b style="font-size:1.5em">.co</b></a> 
		</div> 
		<div class="navbar-header pull-right"> 
			<a class="navbar-brand" href="#" data-toggle="modal" data-target="#myModalLogin" style="color:green" >Login & Register</a> 
		</div> 
		<div class="collapse navbar-collapse" id="example-navbar-collapse"> 
			<ul class="nav navbar-nav"> 
				<li><a href="index.php">Tentang Kami</a></li> 
				<li><a href="#">Order Barber</a></li> 
				<li><a href="#">Promo</a></li> 
				<li class="dropdown"> 
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">seetDahya<b class="caret"></b> </a> 
					<ul class="dropdown-menu"> 
						<li><a href="#">Gratis </a></li> 
						<li><a href="#">EJB</a></li>
						<li><a href="#">Jasper Report</a></li> 
						<li class="divider"></li> 
							<li><a href="#">Separated link</a></li> 
						<li class="divider"></li> 
							<li><a href="#">One more separated link</a></li> 
					</ul> 
					</li> 
			</ul> 
		</div> 
	</nav>
	<?php
	if($_REQUEST['insert'] == "failed")
	{
		?>
		<div class="alert alert-warning"  style="padding:-30px;">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <p style="color:red; font-size:1.2em;" ><i class="glyphicon glyphicon-cross"></i> Maaf, Username dan Password Salah</p>
			  
		</div>
		<?php
	}
	

?>
	
	<div id="myModalLogin" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content center-block" style="width:250px; margin-top:100px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title" style="text-align:center; color:#ff5500;"><b>Welcome,<br>Please Log in</b></h3>
			</div>
	
				<div class="modal-body">
				<form method="post"  action="cek_login.php">
		
				<div class="form-group" >
				
				<label for="firstname" class="lodo">Username</label> 
				<input type="text" class="form-control"
				name="username"
				placeholder="Input Your Username" 
				required oninvalid="this.setCustomValidity
				('Please Input Your Username')" oninput="setCustomValidity('')"> 						
				</div>		
						
						
				<div class="form-group" >
				<label for="lastname" class="lodo">Password</label> 
				<input type="password" class="form-password" 
				name="password"
				id="password" placeholder="Enter Password" 
				required oninvalid="this.setCustomValidity
				('Please Input Your Password')" oninput="setCustomValidity('')"> 
				
				<input type="checkbox" name="checkbox" class="form-checkbox">
				Show Password </input>
				</div> 
		
				<div class="form-group"> 
				<button type="submit" class="btn btn-success">Sign in
				</button>
				<button type="button" class="btn" data-toggle="modal" data-target="#myModal1" style="color:green;">
				 Register </button>
				</div>
	

	</form>
	
	</div>
	</div>
	</div>
	</div>

<div id="myModal1" class="modal fade">
	<div class="modal-dialog center-block" style="width:250px; margin-top:100px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title" style="color:#ff5500; text-align:center;"><b>Please Register</b></h2>
			</div>
			<div class="modal-body">
			
		<form method="POST" name="pendaftaran" action="proses_daftar.php">
							
				<div class="form-group" >
				<label for="firstname" class="lodo">Nama Lengkap</label> 
				<input type="text" class="form-control loko"
				name="nama"
				placeholder="Masukan Nama Lengkap Anda" 
				required oninvalid="this.setCustomValidity
				('Please Input Your Full Name')" oninput="setCustomValidity('')"> 						
				</div> 			
							
				<div class="form-group" >
				<label for="firstname" class="lodo">Username</label> 
				<input type="text" class="form-control loko"
				name="username"
				placeholder="Masukan Username Anda" 
				required oninvalid="this.setCustomValidity
				('Please Input Your Username')" oninput="setCustomValidity('')"> 						
				</div> 
				
				<div class="form-group" >
				<label for="firstname" class="lodo">Email</label> 
				<input type="text" class="form-control loko"
				name="email"
				placeholder="Masukan Email Anda" 
				required oninvalid="this.setCustomValidity
				('Please Input Your Email')" oninput="setCustomValidity('')"> 						
				</div> 
				
				<div class="form-group"> 
				<label for="lastname" class="lodo">Password</label> 
				<input type="password" class="form-password" 
				name="password"
				id="password" placeholder="Enter Password" 
				required oninvalid="this.setCustomValidity
				('Please Input Your Password')" oninput="setCustomValidity('')"> 
				
				<input type="checkbox" name="checkbox" class="form-checkbox"> 
				Show Password </input>
				</div> 
				
				<div class="form-group"> 
				<button type="submit" class="btn btn-success">Register
				</button>
				</div>
				
		</form>	
	</div>
	</div>
	</div>
	</div>

<footer> 
  
  

  
  
  
  
  
  
  
  
  
</body>
</html>
  
