<!DOCTYPE html>
<html>
<head>
	<title> MrTampan.co</title>
	
	<link rel = "stylesheet" type = "text/css" href = "assets/css/bootstrap.css">
		
	<link rel = "stylesheet" type = "text/css" href = "assets/js/jquery-ui/jquery-ui.css">

	<script type="text/javascript" src="assets/js/jquery.js"></script>

	<script type = "text/javascript" src = "assets/js/bootstrap.js"></script>

	<script type = "text/javascript" src = "assets/js/jquery-ui/jquery-ui.js"></script>


</head>

<body  class="body" style="font-family:nyala;">

<div class="container">
  <div class="row">
  <div class="col-md-12">

	<nav class="navbar navbar-default" role="navigation" style="background-color:white; border:none;"> 
		<div class="navbar-header" > 
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
				<span class="sr-only">Menu</span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
			</button> 
			<a class="navbar-brand" href="#" ><b style="font-size:2em; color:#ff5500">MrTampan</b><b style="font-size:1.5em">.co</b></a> 
		</div> 
		<div class="navbar-header pull-right"> 
			<a class="navbar-brand" href="#" data-toggle="modal" data-target="#myModalLogin" style="color:green" >Welcome <?php echo $_SESSION['username']; ?></a> 
		</div> 
		<div class="collapse navbar-collapse" id="example-navbar-collapse"> 
			<ul class="nav navbar-nav"> 
				<li><a href="index.php">Tentang Kami</a></li> 
				<li><a href="#">Order Barber</a></li> 
				<li><a href="#">Promo</a></li> 
				<li class="dropdown"> 
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">seetDahya<b class="caret"></b> </a> 
					<ul class="dropdown-menu"> 
						<li><a href="logout.php">Logout</a></li> 
						<li><a href="#">EJB</a></li>
						<li><a href="#">Jasper Report</a></li> 
						<li class="divider"></li> 
							<li><a href="#">Separated link</a></li> 
						<li class="divider"></li> 
							<li><a href="#">One more separated link</a></li> 
					</ul> 
					</li> 
			</ul> 
		</div> 
	</nav>