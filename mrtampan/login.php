<?php
error_reporting(0);
include "koneksi.php";
session_start();
if (($_SESSION['username'])){
header ("location:index.php");

}
?>
<?php 

include "header.php";

?>


<!DOCTYPE html>
<html>
<head>
	<title> Mrtampan.co </title>
	<script src="dist/sweetalert.min.js"></script>

</head>
<body>


	<div class="container">
		<h1 style="font-size:4em; color:#ff5500"><center> Barber Every Place</center></h1>
		<div class="jumbotron" style="border-radius:10%; margin-top:30px;">
		  <a href="#" data-toggle="modal" data-target="#myModal-ask"><span class="glyphicon glyphicon-question-sign"></span> Bantuan</a>
			<div class="row">
			<div class="col-md-4">
				<h3 class="brand"> Apa itu <b style="color:#ff5500;">Mrtampan</b> ?</h3>
				<p style="font-size:1.1em;">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b style="color:#ff5500;">Mrtampan</b> adalah platform market place yang 
				mempertemukan Barber dan konsumen yag membutuhkan jasa barber. Sekarang dengan <b style="color:#ff5500;">Mrtampan</b> 
				Anda tidak lagi harus ke BarberShop untuk 
				merapikan rambut Anda, cukup Order Barber di <b style="color:#ff5500;">Mrtampan</b>, kami akan segera datang ke tempat Anda.
				<br>Anda ingin merasakan style barber zaman now ? 
				segera klik tombol dibawah ini</p>
				<a class="btn btn-success" onclick="login()"> Order Barber </a>
			</div>
			<div class="col-md-4">
				<h3 class="brand"> Bergabung dengan <b style="color:#ff5500;">Mrtampan</b> ?</h3>
				<p style="font-size:1.1em;">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b style="color:#ff5500;">Mrtampan</b> saat  ini masih membutuhkan banyak barber
				untuk bergabung bersama kami. Saat ini untuk membuka usaha barber tidak perlu lagi modal besar, cukup join bersama 
				<b style="color:#ff5500;">Mrtampan</b>
				Anda sudah menjadi Wirausaha.
				Jika Anda punya keahlian barber dan Anda berniat membuka usaha barber serta ingin mendapat penghasilan, kami hadir untuk Anda.<br>
				Ayo!! segera gabung bersama kami</p>
				<a class="btn btn-primary" onclick="login()"> Jadi Barber </a>
			</div>
			<div class="col-md-4">
				<h3 class="brand"> Berkarir di <b style="color:#ff5500;">Mrtampan</b> ?</h3> 
				<p style="font-size:1.1em;"><b style="color:#ff5500;">Mrtampan</b> merupakan Startup di bidang market place yang baru berdiri,
				kami memiliki misi untuk meningkatkan pertumbuhan ekonomi dengan membantu para barber untuk join bersama kami. Saat ini kami sedang
				membuka lowongan kerja untuk bergabung dan maju bersama kami.  
				</p>
				<a class="btn btn-warning" onclick="login()"> Lihat Karir</a>
			</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<div class="jumbotron" style="background-color:white; border-radius:10%; margin-top:20px; padding-left:0.1px; padding-right:0.1px;">
				<h1 style="color:#ff5500;"><center> Tentukan Gayamu</center></h1><br>
						<div id="myCarousel" class="carousel slide"> <!-- Carousel indicators --> 
							<ol class="carousel-indicators">
								 <li data-target="#myCarousel" data-slide-to="0" class="active"></li> 
								 <li data-target="#myCarousel" data-slide-to="1"></li> 
								 <li data-target="#myCarousel" data-slide-to="2"></li> 
								 <li data-target="#myCarousel" data-slide-to="3"></li> 
								 <li data-target="#myCarousel" data-slide-to="4"></li> 
								 <li data-target="#myCarousel" data-slide-to="5"></li> 
							</ol> <!-- Carousel items --> 
						 
						 <div class="carousel-inner"> 
							<div class="item active">
								<center><ul class="list-inline" alt="First Slide">
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut.2.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut.2.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
								</ul></center>
							</div> 
								
							<div class="item">
								<center><ul class="list-inline" alt="Second Slide">
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
								</ul></center>
							</div> 
							
							<div class="item">
								<center><ul class="list-inline" alt="Third Slide">
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut.2.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut.2.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
								</ul></center>
							</div> 
							
							<div class="item">
								<center><ul class="list-inline" alt="Fourth Slide">
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
								</ul></center>
							</div> 
							
							<div class="item">
								<center><ul class="list-inline" alt="Fifth Slide">
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut.2.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut.2.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut1.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
								</ul></center>
							</div> 
							
							<div class="item">
								<center><ul class="list-inline" alt="Sixth Slide">
									<li><a href="#"><img src="image/rambut.2.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut.2.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut.2.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut.2.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
									<li><a href="#"><img src="image/rambut.2.jpg" class="img-responsive"alt="Responsive image Generic placeholder thumbnail"></a></li>
								</ul></center>
							</div> 
														
						</div> <!-- Carousel nav --> 						 
								 <a class="carousel-control left" href="#myCarousel" data-slide="prev"></a> 
								 <a class="carousel-control right" href="#myCarousel" data-slide="next"></a> 
						</div>
								<center><h2><button class="btn btn-default" href="#" style="font-size:em; background-color:#ff5500; color:white;"> Gaya Rambut Lainnya </button></h2></center>
			</div>
		</div>
		</div>
	</div>
	
		
		<div id="myModal-ask" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content center-block" style=" width:280px; margin-top:100px;">
			<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<ul class="list-inline" id="myTab" style="text-align:center;"> 
						<li class="dropdown"> 
							<a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown">Pilih Bantuan <b class="caret"></b></a> 
							<ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1"> 
								<li><a href="#order_barber" tabindex="-1" data-toggle="tab">Cara Pesan Barber ?</a></li> 
								<li><a href="#jadi_barber" tabindex="-1" data-toggle="tab">Cara Menjadi Barber ?</a></li> 
								<li><a href="#faq" tabindex="-1" data-toggle="tab">FAQ</a></li> 
							</ul> 
						</li> 
					</ul> 
			</div>	
			<div class="modal-body">
					<div id="myTabContent" class="tab-content"> 
						<div class="tab-pane fade in active" id="order_barber"> 
						<p style="text-align:center;"> Panduan Cara Memesan Barber</p> 
						<ol>
							<li> Pastikan Anda sudah Login ke Mrtampan.co </li>
							<li> Pilih <b class="btn btn-success">Order Barber</b></li>
							<li> Ikuti tahapan yang tersedia</li>
							<li> Masukan No HP yang aktif dan bisa ditelpon</li>
							<li> Tunggu SMS dalam beberapa menit</li>
						</ol>
						</div> 
						<div class="tab-pane fade" id="jadi_barber"> 
						<p style="text-align:center;"> Panduan Cara Menjadi Barber</p> 
						<ol>
							<li> Pastikan Anda sudah Login ke Mrtampan.co </li>
							<li> Pilih <b class="btn btn-primary">Jadi Barber</b></li>
							<li> Masukan NIK KTP Anda</li>
							<li> Ikuti tahapan yang tersedia</li>
							<li> Masukan No HP yang aktif dan bisa ditelpon</li>
							<li> Permohonan Anda akan diproses 3 hari kerja</li>
						</ol>
						</div> 
						
						<div class="tab-pane fade" id="faq"> 
						<p style="text-align:center;">Yang Sering Ditanyakan..</p> 
						<ol>
							<li><b> Tanya </b> : Pastikan Anda sudah Login ke Mrtampan.co <br>
								<b>	Jawab </b> : Pastikan Anda sudah Login ke Mrtampan.co </li>
							<li><b> Tanya </b> : Pastikan Anda sudah  <br>
								<b>	Jawab </b> : Pastikan Anda sudah Login ke Mrtampan.co </li>
							<li><b> Tanya </b> : Pastikan Anda sudah Login ke Mrtampan.co <br>
								<b>	Jawab </b> : Pastikan Anda  </li>
							<li><b> Tanya </b> : Pastikan Anda sudah Login ke Mrtampan.co <br>
								<b>	Jawab </b> : Pastikan Anda sudah Login ke Mrtampan.co </li>
						</ol>
						</div> 
					</div> 
		</div>
		</div>
	</div>
	</div>
	
<script language="Javascript">
function login()
{
swal("Maaf Sekali!", "Anda Harus Login Terlebih Dahulu!", "info", {button:"Okehh",});
}
</script>

					




<?php

include "footer.php";

?>